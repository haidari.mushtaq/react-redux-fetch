import { combineReducers ,createStore } from "redux"
import {userReducer, postReducer} from './reducers';
//all reducers combined
export const combinedReducers = combineReducers({userReducer, postReducer})
//store
export const store = createStore(combinedReducers);
