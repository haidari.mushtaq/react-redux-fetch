import * as actions from './actions';
//reducers
export const userReducer = (state = { 
payload: {
    users:[]
} }, action) => {
	const newState = { ...state }
	switch (action.type) {
		case actions.GET_USERS:
			newState.payload.users = action.payload.users
			return newState
		default:
			return newState
	}
}

export const postReducer = (state = { payload: {
    posts: []
} }, action) => {
	const newState = { ...state }
	switch (action.type) {
		case actions.GET_POSTS:
			newState.payload.posts = action.payload.posts
			return newState
		default:
			return newState
	}
}