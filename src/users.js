import * as React from 'react'
import { connect } from 'react-redux'
import * as action from './actions'

class Users extends React.Component {
	renderArrayProps = () => {
		const users = this.props.users
		return users !== (null || undefined) && users.length > 0
			? users.map(p => (
					<div key={p.id} className="post-card">
						<p>
							<span className="id">{p.id}</span>
							<br />
							Name: {p.name}
							<br />
							website: {p.website}
							<br />
							address: {p.address.city}
						</p>
					</div>
				))
			: <div className="alert">no users loaded yet</div>;
	}
	render() {
		return (
			<div className="wrapper">
				<div>{this.renderArrayProps()}</div>
				<button onClick={action.getUsers.bind(this)}>Get Users</button>
			</div>
		)
	}
}
const stateToProps = state => ({
	users: state.userReducer.payload.users
})
export default connect(stateToProps)(Users)
